package com.locus.authsystem.resources;

import com.google.inject.Inject;
import com.locus.authsystem.domain.User;
import com.locus.authsystem.services.UserService;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Path("/v1/authsystem/")
@Slf4j
public class UserResource {

    private UserService userService;

    @Inject
    public UserResource(UserService userService) {
        this.userService = userService;
    }


    @Path("/addUser/")
    //@CheckHeaderFilter
    public Response addUser(User user) {
        try {
            userService.createUser(user);
            return Response.ok().build();
        } catch (Exception ex) {
            log.error("Unable add user: {}", user.toString());
            throw ex;
        }
    }
}
