package com.locus.authsystem.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class MapConverterUtil {
    public static <T> T convertMapToObject(Map<String, Object> request, Class<T> cls) {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.convertValue(request, cls);
    }
}
