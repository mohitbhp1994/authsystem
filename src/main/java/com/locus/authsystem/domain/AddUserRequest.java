package com.locus.authsystem.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Builder
@Data
public class AddUserRequest {
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("user_name")
    private String userName;
    @JsonProperty("user_type")
    private Enums.UserType userType;
    @JsonProperty("resource_types")
    private List<Enums.ResourceType> resourceTypes;
    @JsonProperty("added_by_user_id")
    private Long addedByUserId;
}
