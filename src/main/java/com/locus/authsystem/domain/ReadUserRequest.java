package com.locus.authsystem.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Builder
@Data
public class ReadUserRequest {
    @JsonProperty("read_by_id")
    private Long readById;

    @JsonProperty("ids_to_be_fetched")
    private List<Long> ids;

    @JsonProperty("all_required")
    private boolean isAllRequired;
}
