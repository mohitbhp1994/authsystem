package com.locus.authsystem.domain;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.locus.authsystem.domain.Enums.Permission.read;
import static com.locus.authsystem.domain.Enums.Permission.sudo;
import static com.locus.authsystem.domain.Enums.Permission.update;
import static com.locus.authsystem.domain.Enums.Permission.write;
import static com.locus.authsystem.domain.Enums.ResourceType.library;
import static com.locus.authsystem.domain.Enums.ResourceType.studentDB;
import static com.locus.authsystem.domain.Enums.Status.active;

public class ConstantUsers {

    public static Map<Enums.UserType, List<Enums.Permission>> userTypeAccessPermissionMap = getAccessPermissions();
    public static Map<Enums.UserType, List<Enums.ResourceType>> userTypeResourcePermissionMap = getResourcePermissions();

    private static Map<Enums.UserType, List<Enums.Permission>> getAccessPermissions() {
        Map<Enums.UserType, List<Enums.Permission>> userTypesListMap = new HashMap<>();

        userTypesListMap.put(Enums.UserType.superAdmin, Arrays.asList(sudo, read, write, update));
        userTypesListMap.put(Enums.UserType.admin, Arrays.asList(read, write, update));
        userTypesListMap.put(Enums.UserType.manager, Arrays.asList(read, write));
        userTypesListMap.put(Enums.UserType.user, Collections.singletonList(read));

        return userTypesListMap;
    }

    private static Map<Enums.UserType, List<Enums.ResourceType>> getResourcePermissions() {
        Map<Enums.UserType, List<Enums.ResourceType>> userTypesListMap = new HashMap<>();

        userTypesListMap.put(Enums.UserType.superAdmin, Arrays.asList(library, studentDB));
        userTypesListMap.put(Enums.UserType.admin, Arrays.asList(library, studentDB));
        userTypesListMap.put(Enums.UserType.manager, Arrays.asList(library, studentDB));
        userTypesListMap.put(Enums.UserType.user, Collections.singletonList(library));

        return userTypesListMap;
    }

    /*
    * In memory Users
    * */
    public static Map<Long, User> getPreloadedUsers() {
        Map<Long, User> userMap = new HashMap<>();
        userMap.put(1L, createUser(1L, 1L, getResourcePermissions().get(Enums.UserType.superAdmin), Enums.UserType.superAdmin, active));
        userMap.put(2L, createUser(2L, 1L, getResourcePermissions().get(Enums.UserType.admin), Enums.UserType.admin, active));
        userMap.put(3L, createUser(3L, 2L, getResourcePermissions().get(Enums.UserType.manager), Enums.UserType.manager, active));
        userMap.put(4L, createUser(4L, 2L, getResourcePermissions().get(Enums.UserType.manager), Enums.UserType.manager, active));
        userMap.put(5L, createUser(5L, 3L, getResourcePermissions().get(Enums.UserType.user), Enums.UserType.user, active));
        userMap.put(6L, createUser(6L, 3L, getResourcePermissions().get(Enums.UserType.user), Enums.UserType.user, active));
        userMap.put(7L, createUser(7L, 4L, getResourcePermissions().get(Enums.UserType.user), Enums.UserType.user, active));

        return userMap;

    }

    public static User createUser(Long id, Long addedById, List<Enums.ResourceType> allowedResourceTypes,
                                  Enums.UserType userType, Enums.Status status) {
        return User.builder()
                .addedById(addedById)
                .firstName("first_name_" + id)
                .lastName("last_name_" + id)
                .resourceType(allowedResourceTypes)
                .userType(userType)
                .status(status)
                .build();
    }
}
