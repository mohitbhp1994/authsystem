package com.locus.authsystem.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;
import com.locus.authsystem.exceptions.AuthException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.core.Response;
import java.util.List;

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {
    @JsonProperty("id")
    private Long userId;

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("type")
    private Enums.UserType userType;

    @JsonProperty("resources")
    private List<Enums.ResourceType> resourceType;

    @JsonProperty("added_by_id")
    private Long addedById;

    @JsonProperty("status")
    private Enums.Status status;

    /*
    * Validate User Object request
    * Add multiple methods here for future Validations
    * */
    public void validate() {
        if (Strings.isNullOrEmpty(this.firstName) || this.firstName.length() > 20) {
            throw AuthException.builder()
                    .header("BAD Request")
                    .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                    .message("Please check firstName. Length should be between [1,20]").build();
        } else if (Strings.isNullOrEmpty(this.lastName) || this.lastName.length() > 20) {
            throw AuthException.builder()
                    .header("BAD Request")
                    .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                    .message("Please check lastName. Length should be between [1,20]").build();
        }
    }
}
