package com.locus.authsystem.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Builder
@Data
public class UpdateUserRequest {
    @JsonProperty("user_to_update")
    private Long userToUpdateId;

    @JsonProperty("field_to_update")
    private String fieldToUpdated;

    @JsonProperty("updated_values")
    private Object updatedValues;

    @JsonProperty("updated_by_id")
    private Long updatedById;
}
