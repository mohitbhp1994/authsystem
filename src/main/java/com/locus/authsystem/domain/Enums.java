package com.locus.authsystem.domain;

public interface Enums {
    enum UserType {
        superAdmin, admin, manager, user
    }

    enum ResourceType {
        library, studentDB, all
    }

    enum Permission {
        sudo, read, write, update
    }

    enum UserCommand {
        add, delete, read
    }

    enum Status {
        active, inactive, pending
    }
}
