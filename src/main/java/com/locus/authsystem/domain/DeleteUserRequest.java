package com.locus.authsystem.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Builder
@Data
public class DeleteUserRequest {
    @JsonProperty("deleted_by_id")
    private Long deletedById;
    @JsonProperty("to_be_deleted_id")
    private Long toBedeleteId;
}
