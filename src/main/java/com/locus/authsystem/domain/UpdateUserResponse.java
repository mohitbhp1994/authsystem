package com.locus.authsystem.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Builder
@Data
public class UpdateUserResponse {
    @JsonProperty("is_updated")
    private boolean isUpdated;

    @JsonProperty("updated_user")
    private User updatedUser;
}
