package com.locus.authsystem.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Data
@Builder
public class UserCommandRequest {
    private Enums.UserCommand command;
    private Map<String, Object> request;
    private String source;
}
