package com.locus.authsystem.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Builder
@Data
public class DeleteUserResponse {
    @JsonProperty("is_user_deleted")
    private boolean isUserDeleted;

    @JsonProperty("deleted_user_request")
    private DeleteUserRequest deleteUserRequest;

}
