package com.locus.authsystem.setup;

import com.google.inject.Inject;
import com.locus.authsystem.configuration.DataBaseConfiguration;
import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;

public class DataSourceCreater {

    private DataBaseConfiguration dataBaseConfiguration;

    @Inject
    private DataSourceCreater(DataBaseConfiguration dataBaseConfiguration) {
        this.dataBaseConfiguration = dataBaseConfiguration;
    }

    public DataSource setupDatabase() {
        final HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setDriverClassName(dataBaseConfiguration.getDriverClassName());
        hikariDataSource.setJdbcUrl(dataBaseConfiguration.getUrl());
        hikariDataSource.setUsername(dataBaseConfiguration.getUserName());
        hikariDataSource.setPassword(dataBaseConfiguration.getPassword());

        hikariDataSource.setConnectionTestQuery(dataBaseConfiguration.getConnectionTestQuery());

        return hikariDataSource;
    }
}
