package com.locus.authsystem.factories;

import com.locus.authsystem.domain.Enums;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.usertype.UserType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Slf4j
public class UserTypePermissionFactory {
    private Map<UserType, List<Enums.Permission>> userTypeToPermissionTypesMap = new HashMap<>();

    public void addUserTypePermissionType(UserType userType, List<Enums.Permission> permissions) {
        userTypeToPermissionTypesMap.put(userType, permissions);
    }

    public Map<UserType, List<Enums.Permission>> getUserTypeToPermissionMap() {
        return userTypeToPermissionTypesMap;
    }

    public void removeUserTypeToPermission(UserType userType, Enums.Permission permissionToBeRemoved) {
        if (Objects.nonNull(userTypeToPermissionTypesMap.get(userType))) {
            List<Enums.Permission> permissions = userTypeToPermissionTypesMap.get(userType);
            int index = permissions.indexOf(permissionToBeRemoved);
            if (index != -1) {
                permissions.remove(index);
                userTypeToPermissionTypesMap.put(userType, permissions);
            } else {
                log.error("Unable to remove element as no element is present: to be removed: {}, list: {}",
                        permissionToBeRemoved, permissions);
            }
        } else {
            log.error("User type not found in userTypeToPermissionTypesMap: {}", userType);
        }
    }
}
