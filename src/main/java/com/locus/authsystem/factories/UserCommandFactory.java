package com.locus.authsystem.factories;

import com.locus.authsystem.domain.Enums;
import com.locus.authsystem.exceptions.AuthException;
import com.locus.authsystem.processors.UserCommandProcessor;

import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class UserCommandFactory {
    private static Map<Enums.UserCommand, UserCommandProcessor> userCommandUserProcessorMap = new HashMap<>();

    public static void addAllProcessors(List<UserCommandProcessor> userProcessors) {
        userProcessors.forEach(processor -> userCommandUserProcessorMap.put(processor.getUserCommand(), processor));
    }

    public static void addProcessor(UserCommandProcessor userProcessor) {
        userCommandUserProcessorMap.put(userProcessor.getUserCommand(), userProcessor);
    }

    public static void removeProcessor(UserCommandProcessor userProcessor) {
        userCommandUserProcessorMap.remove(userProcessor.getUserCommand());
    }

    public static UserCommandProcessor getCommandProcessor(Enums.UserCommand userCommand) {
        return Optional.ofNullable(userCommandUserProcessorMap.get(userCommand)).orElseThrow(() ->
                AuthException.builder()
                        .statusCode(Response.Status.BAD_REQUEST.getStatusCode())
                        .header("BAD Request")
                        .message("Not command found with name: " + userCommand.name())
                        .build());
    }
}
