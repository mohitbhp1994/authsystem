package com.locus.authsystem.factories;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.usertype.UserType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.locus.authsystem.domain.Enums.ResourceType;

@Slf4j
public class UserTypeResourceFactory {
    private Map<UserType, List<ResourceType>> userTypeToResourceTypesMap = new HashMap<>();

    public void addUserTypeResourceType(UserType userType, List<ResourceType> resourceTypes) {
        userTypeToResourceTypesMap.put(userType, resourceTypes);
    }

    public Map<UserType, List<ResourceType>> getUserTypeToResourceTypesMap() {
        return userTypeToResourceTypesMap;
    }

    public void removeUserTypeToResource(UserType userType, ResourceType resourceTypeToRemove) {
        if (Objects.nonNull(userTypeToResourceTypesMap.get(userType))) {
            List<ResourceType> resourceTypes = userTypeToResourceTypesMap.get(userType);
            int index = resourceTypes.indexOf(resourceTypeToRemove);
            if (index != -1) {
                resourceTypes.remove(index);
                userTypeToResourceTypesMap.put(userType, resourceTypes);
            } else {
                log.error("Unable to remove element as no element is present: {}", resourceTypes);
            }
        } else {
            log.error("User type not found in userTypeToResourceTypesMap: {}", userType);
        }
    }
}
