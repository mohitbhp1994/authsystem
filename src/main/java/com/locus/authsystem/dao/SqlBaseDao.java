package com.locus.authsystem.dao;

import java.util.List;

public abstract class SqlBaseDao<K, V> {
    public abstract V fetchById(Long id);
    public abstract boolean deleteUser(Long id);
    public abstract V insert(K object);
    public abstract List<V> fetchByIds(List<Long> ids);

}
