package com.locus.authsystem.dao;

import com.google.inject.Inject;
import com.locus.authsystem.domain.User;
import com.locus.authsystem.exceptions.AuthException;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.locus.authsystem.domain.ConstantUsers.createUser;
import static com.locus.authsystem.domain.ConstantUsers.getPreloadedUsers;
import static com.locus.authsystem.domain.Enums.Status.active;

@Slf4j
public class UserDao extends SqlBaseDao<User, User> {

    private static Map<Long, User> userMap;
    private DataSource dataSource;


    @Inject
    public UserDao(DataSource dataSource) {
        this.dataSource = dataSource;
        userMap = getPreloadedUsers();
    }

    @Override
    public User fetchById(Long userId) {
        User user = userMap.get(userId);
        if (Objects.isNull(user)) {
            throw AuthException.builder()
                    .statusCode(Response.Status.NOT_FOUND.getStatusCode())
                    .header("User Not Found")
                    .message("No user exists with id: " + userId)
                    .build();
        }
        return user;
    }


    @Override
    public boolean deleteUser(Long id) {
        return false;
    }

    @Override
    public User insert(User user) {
        Long maxKey = userMap.entrySet().stream().max((entry1, entry2) -> entry1.getKey() > entry2.getKey() ? 1 : -1).get().getKey();
        userMap.put(maxKey + 1, createUser(maxKey + 1, user.getAddedById(), user.getResourceType(), user.getUserType(), active));
        user.setUserId(maxKey + 1);
        return user;
    }

    @Override
    public List<User> fetchByIds(List<Long> ids) {
        List<User> users = new ArrayList<>();
        ids.forEach(id -> {
            try {
                User user = fetchById(id);
                users.add(user);
            } catch (Exception ex) {
                log.error("Unabl to fetch user withId: {}, exception: {}", id, ex.getMessage());
            }
        });
        return users;
    }

    public List<User> fetchAllUsers() {
        List<User> users = new ArrayList<>();
        userMap.entrySet().forEach(k -> users.add(k.getValue()));
        return users;
    }

}
