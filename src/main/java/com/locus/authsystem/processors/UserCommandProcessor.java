package com.locus.authsystem.processors;

import com.locus.authsystem.domain.Enums.UserCommand;
import lombok.Data;

@Data
public abstract class UserCommandProcessor<REQ, RES> {
    private UserCommand userCommand;

    abstract RES execute(REQ userCommandRequest);
}