package com.locus.authsystem.processors;

import com.locus.authsystem.domain.DeleteUserRequest;
import com.locus.authsystem.domain.DeleteUserResponse;
import com.locus.authsystem.domain.Enums;
import com.locus.authsystem.services.UserService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DeleteUserCommandProcessor extends UserCommandProcessor<DeleteUserRequest, DeleteUserResponse> {

    private UserService userService;
    private Enums.UserCommand command;

    public DeleteUserCommandProcessor(UserService userService, Enums.UserCommand command) {
        this.userService = userService;
        this.command = command;
    }


    @Override
    DeleteUserResponse execute(DeleteUserRequest deleteUserRequest) {
        log.info("Executing command: {}, request: {}", this.command, deleteUserRequest.toString());
        boolean isUserDeleted = userService.deleteUser(deleteUserRequest);
        return DeleteUserResponse.builder().isUserDeleted(isUserDeleted).deleteUserRequest(deleteUserRequest).build();
    }
}
