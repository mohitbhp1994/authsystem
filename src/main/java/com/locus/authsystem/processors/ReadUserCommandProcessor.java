package com.locus.authsystem.processors;

import com.locus.authsystem.domain.Enums;
import com.locus.authsystem.domain.ReadUserRequest;
import com.locus.authsystem.domain.ReadUserResponse;
import com.locus.authsystem.domain.User;
import com.locus.authsystem.services.UserService;
import lombok.extern.slf4j.Slf4j;

import java.util.List;


@Slf4j
public class ReadUserCommandProcessor extends UserCommandProcessor<ReadUserRequest, ReadUserResponse> {
    private UserService userService;
    private Enums.UserCommand command;

    public ReadUserCommandProcessor(UserService userService, Enums.UserCommand command) {
        this.userService = userService;
        this.command = command;
    }


    @Override
    ReadUserResponse execute(ReadUserRequest readUserRequest) {
        log.info("Executing command: {}, request: {}", this.command, readUserRequest.toString());
        List<User> fetchedUsers = userService.fetchUsers(readUserRequest.getReadById(), readUserRequest.getIds(), readUserRequest.isAllRequired());
        return ReadUserResponse.builder().users(fetchedUsers).build();
    }
}
