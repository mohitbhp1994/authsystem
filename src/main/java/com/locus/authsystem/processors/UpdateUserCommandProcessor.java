package com.locus.authsystem.processors;

import com.locus.authsystem.domain.Enums;
import com.locus.authsystem.domain.UpdateUserRequest;
import com.locus.authsystem.domain.UpdateUserResponse;
import com.locus.authsystem.domain.User;
import com.locus.authsystem.services.UserService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UpdateUserCommandProcessor extends UserCommandProcessor<UpdateUserRequest, UpdateUserResponse> {
    private UserService userService;
    private Enums.UserCommand command;

    public UpdateUserCommandProcessor(UserService userService, Enums.UserCommand command) {
        this.userService = userService;
        this.command = command;
    }

    @Override
    UpdateUserResponse execute(UpdateUserRequest updateUserRequest) {
        log.info("Executing command: {}, request: {}", this.command, updateUserRequest.toString());
        User updateUser = userService.updateUser(updateUserRequest);
        return UpdateUserResponse.builder().isUpdated(true).updatedUser(updateUser).build();
    }
}
