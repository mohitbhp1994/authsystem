package com.locus.authsystem.processors;

import com.locus.authsystem.domain.AddUserResponse;
import com.locus.authsystem.domain.Enums;
import com.locus.authsystem.domain.User;
import com.locus.authsystem.services.UserService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AddUserCommandProcessor extends UserCommandProcessor<User, AddUserResponse> {

    private UserService userService;
    private Enums.UserCommand command;

    public AddUserCommandProcessor(UserService userService, Enums.UserCommand command) {
        this.userService = userService;
        this.command = command;
    }

    @Override
    AddUserResponse execute(User addUserRequest) {
        log.info("Executing command: {}, request: {}", this.command, addUserRequest.toString());
        boolean isUserAdded = this.userService.createUser(addUserRequest);
        return AddUserResponse.builder().isAdded(isUserAdded).userRequest(addUserRequest).build();
    }
}
