package com.locus.authsystem.services;

import com.google.inject.Inject;
import com.locus.authsystem.dao.UserDao;
import com.locus.authsystem.domain.DeleteUserRequest;
import com.locus.authsystem.domain.Enums;
import com.locus.authsystem.domain.UpdateUserRequest;
import com.locus.authsystem.domain.User;
import com.locus.authsystem.exceptions.AuthException;
import lombok.extern.slf4j.Slf4j;

import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;

import static com.locus.authsystem.domain.ConstantUsers.userTypeAccessPermissionMap;
import static com.locus.authsystem.domain.ConstantUsers.userTypeResourcePermissionMap;

@Slf4j
public class UserService {

    private UserDao userDao;

    @Inject
    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

    /*
    * This is for creating new user
    * 1. First check user adding user is having permission to add
    * 2. Check if not already created
    * */
    public boolean createUser(User user) {
        log.info("Fetch user details from db of user: {}", user.getAddedById());
        User fetchedAdminOrManager = userDao.fetchById(user.getAddedById());
        if (isPermissionGivenForCreateUser(fetchedAdminOrManager, user)) {
            try {
                userDao.insert(user);
            } catch (AuthException ex) {
                log.error("Unable to create user: {}, exception: {}", user.toString(), ex.getMessage());
            }
        } else {
            throw AuthException.builder()
                    .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                    .header("Access Forbidden")
                    .message("User not allowed to add new user")
                    .build();
        }
        return true;
    }

    private boolean isPermissionGivenForCreateUser(User fetchedAdminOrManager, User userToBeCreated) {
        return Objects.nonNull(userTypeResourcePermissionMap.get(fetchedAdminOrManager.getUserType())) &&
                userTypeResourcePermissionMap.get(fetchedAdminOrManager.getUserType()).containsAll(userToBeCreated.getResourceType()) &&
                Objects.nonNull(userTypeAccessPermissionMap.get(fetchedAdminOrManager.getUserType())) &&
                (userTypeAccessPermissionMap.get(fetchedAdminOrManager.getUserType())).contains(Enums.Permission.write);
    }

    private boolean isPermissionGivenForReadUser(User fetchedAdminOrManager) {
        return Objects.nonNull(userTypeAccessPermissionMap.get(fetchedAdminOrManager.getUserType())) &&
                (userTypeAccessPermissionMap.get(fetchedAdminOrManager.getUserType())).contains(Enums.Permission.read);
    }

    public List<User> fetchUsers(Long readById, List<Long> ids, boolean allRequired) {
        User fetchedAdminOrManager = userDao.fetchById(readById);
        if (isPermissionGivenForReadUser(fetchedAdminOrManager)) {
            return allRequired ? userDao.fetchAllUsers() : userDao.fetchByIds(ids);
        } else {
            throw AuthException.builder()
                    .statusCode(Response.Status.FORBIDDEN.getStatusCode())
                    .header("Access Forbidden")
                    .message("User not allowed to read user")
                    .build();
        }
    }

    /*TODO*/
    public boolean deleteUser(DeleteUserRequest deleteUserRequest) {
        return false;
    }

    public User updateUser(UpdateUserRequest updateUserRequest) {
        return null;
    }


}
