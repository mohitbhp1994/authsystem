package com.locus.authsystem.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import java.util.HashMap;
import java.util.Map;

public class AuthExceptionMapper implements ExceptionMapper<AuthException> {
    @Override
    public Response toResponse(AuthException e) {
        Map<String, String> exception = new HashMap<>();
        exception.put("header", e.getHeader());
        exception.put("message", e.getMessage());
        return Response.status(e.getStatusCode()).entity(exception).build();
    }
}
