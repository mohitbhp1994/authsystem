package com.locus.authsystem.exceptions;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Data
@Builder
public class AuthException extends RuntimeException {
    private int statusCode;
    private String header;
    private String message;

}
