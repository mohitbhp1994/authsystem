package com.locus.authsystem.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataBaseConfiguration {
    private String driverClassName;
    private String url;
    private String userName;
    private String password;
    private String connectionTestQuery;
}