package com.locus.authsystem.configuration;

import io.dropwizard.Configuration;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Data
public class AppConfig extends Configuration {
    private DataBaseConfiguration dataBaseConfiguration;
}
